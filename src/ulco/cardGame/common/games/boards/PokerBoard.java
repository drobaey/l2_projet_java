package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PokerBoard implements Board {
    private List<Card>cards;
    private List<Coin>coins;

    public PokerBoard() {
        cards=new ArrayList<>();
        coins=new ArrayList<>();
    }

    public void clear() {
        cards.clear();
        coins.clear();
    }

    public void addComponent(Component component) {
        if (component instanceof Card) {
            cards.add((Card)component);
        } else {
            coins.add((Coin)component);
        }
    }

    public List<Component> getComponents() {
       List<Component> list = new ArrayList<Component>();
       list.addAll(cards);
       list.addAll(coins);

       return list;
    }

    public List<Component> getSpecificComponents(Class<? extends Component> classType) {
        if (classType == Card.class) {
            return new ArrayList<>(cards);
        } else {
            return new ArrayList<>(coins);
        }
    }

    public void displayState() {
        System.out.println("----------------Board State----------------");

        for (Card c : cards) {
            System.out.println("- Card: " + c.getName());
        }

        System.out.println("                ------------               ");

        List<String> listColor = new ArrayList<>();
        int sum=0;
        for (Coin c : coins) {
            listColor.add(c.getName());
            sum+=c.getValue();
        }

        System.out.println("- Coin red x " + Collections.frequency(listColor,"Red"));
        System.out.println("- Coin blue x " + Collections.frequency(listColor,"Blue"));
        System.out.println("- Coin black x " + Collections.frequency(listColor,"Black"));

        System.out.println("Your Coins sum placed is about: " + sum);
        System.out.println("-------------------------------------------");
    }
}
