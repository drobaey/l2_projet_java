package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.players.BoardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.Serializable;
import java.net.Socket;
import java.util.List;
import java.util.Map;

public interface Game extends Serializable {

    /**
     * Initialize the whole game using a parameter file
     * @param filename
     */
    void initialize(String filename);

    /**
     *
     * @return
     */
    Player run(Map<Player,Socket> map);

    /**
     * Add player to the current Game
     * @param player
     */
    boolean addPlayer(Socket socket,Player player);

    public Player getCurrentPlayer(String name);

    /**
     * Remove player from the game using the reference
     * @param player
     */
    void removePlayer(Player player);

    /**
     * Remove players from the game
     */
    void removePlayers();

    /**
     * Return player from current Game
     * @return
     */
    List<BoardPlayer> getPlayers();

    /**
     * Specify if game is started or not
     * @return
     */
    boolean isStarted();

    /**
     * current number of players inside the Game
     * @return
     */
    Integer maxNumberOfPlayers();

    /**
     * Specify if the Game has end or not
     */
    boolean end();

    /**
     * Display current Game state
     * Such as user state (name and score)
     */
    void displayState();

    Board getBoard();
}
